import './App.css';

import MyComponent from './components/MyComponent';

import {useState} from "react";
import Title from './components/Title';

function App() {

  const n = 15;
  const [name] = useState("Fernando");
  const redTitle = true;
  return (
    <div className="App">
      {/* Global CSS */}
      <h1>React with CSS</h1>
      {/* Component CSS */}
      <MyComponent />
      <p>This is the App paragraph</p>
      {/* Inline CSS */}
      <p style={{color: "magenta", padding: "25px", borderTop: "2px solid red"}}>
        This element was styled inline
      </p>
      <p style={{color: "magenta", padding: "25px", borderTop: "2px solid red"}}>
        This element was styled inline
      </p>
      {/* Dynamic CSS Inline */}
      <h2 style={n < 10 ? {color: "purple"} : {color: "pink"}}>
        Dynamic CSS
      </h2>
      <h2 style={n > 10 ? {color: "purple"} : {color: "pink"}}>
        Dynamic CSS
      </h2>
      <h2 style={
          name === "Fernando"
            ? {color: "green", backgroundColor: "#000"}
            : null
        }
      >
        Name's test
      </h2>
      {/* Dynamic Class */}
      <h2 className={redTitle ? "red-title" : "title"}>This title has a dynamic class!</h2>
      {/* CSS Modules */}
      <Title />
      <h2 className='my_title'>Testing</h2>
    </div>
  );
}

export default App;
