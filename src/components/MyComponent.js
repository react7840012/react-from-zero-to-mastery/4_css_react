import './MyComponent.css';

const MyComponent = () => {
  return (
    <div>
        <h1>Component's CSS</h1>
        <p>This is a component's paragraph</p>
        <p class="my-comp-paragraph">This is also a component's paragraph</p>
    </div>
  )
}

export default MyComponent